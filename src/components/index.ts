import Vue from 'vue'
import HelloWorld from './HelloWorld.vue'

const Components: any = {
  HelloWorld,
}

Object.keys(Components).forEach((name: string) => {
  Vue.component(name, Components[name])
})

export default Components
